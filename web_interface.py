from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, send_from_directory
import zipfile
import shutil

from student_data import *

# TODO update solutions_wo_results
# TODO show all and id values

# configuration

#DATABASE = '/tmp/flaskr.db'
DEBUG = True
SECRET_KEY = 'acdafda9c92bd9a8de8ffaadabfcecadbfd48dde9dc59fae8dec6adfb'
dev = False
UPLOAD_FOLDER = 'uploads'

app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

#HOST = "192.168.33.10"
HOST= "134.74.146.30"
#HOST = ""

# Session variables
# session["logged_in"]
# session["username"]

# users = []
# exercises = []
# solutions = []
# solutions_wo_results = []


# routing
@app.route("/", methods=['GET', 'POST'])
def login():
	try:
		if session['logged_in']:
			return redirect(url_for("home"))
	except:
		""
	
	error = None
	if request.method == "POST" and not dev:
		get_exercises = BASE_URL+'/exercise'
		temp_username = request.form['username']
		temp_password = request.form['password']
		r=requests.get(get_exercises, verify = False, auth=(temp_username, temp_password))
		
		if r.status_code == 200:
			session['logged_in'] = True
			session['username'] = temp_username
			exercise_list = r.json()['data']['exercises']
		
			for e in exercise_list:
				temp_exercise = Exercise(e['name'])
				temp_exercise.id = e['exercise_id']
				temp_exercise.ac_token = e['access_token']
				exercises.append(temp_exercise)
			
			Populate_data(exercises)
			
			return redirect(url_for("home"))
		elif r.status_code == 502:
			error = "Error reaching Aurum"
		elif r.status_code == 401:
			error = "Incorrect username or password"
		else:
			error = "Authentication failed. Status code: " + str( r.status_code) 
	
	if dev and request.method == "POST":
		session["logged_in"] = True
		session["username"] = request.form["username"]
		return redirect(url_for("home"))
	
	return render_template('login.html', error=error)
	

@app.route("/logout")
def logout():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
		
	session["logged_in"] = False
	session["username"] = ""
	return render_template('logout.html')
	

@app.route("/home")
def home():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
	
	exercise_dict = {}
	user_dict = {}
	
	for e in exercises:
		temp_score = 0
		temp_total = 0
		for s in e.solutions:
			temp_result = s.get_latest_result()
			if temp_result:
				temp_score += temp_result["report"]["stats"]["score"]
				temp_total += temp_result["report"]["stats"]["full_score"]
		
		if len(e.solutions):
			exercise_dict[e.id] = {"name" : e.name, "num_students" : len(e.solutions),
									"average_score": temp_score/len(e.solutions),
									"total_score" : temp_total/len(e.solutions)}
		else:
			exercise_dict[e.id] = {"name" : e.name, "num_students" : 0,
									"average_score": 0, "total_score" : 0}

	for u in users:
		temp_score = 0
		temp_total = 0
		for s in u.solutions:
			temp_result = s.get_latest_result()
			if temp_result:
				temp_score += temp_result["report"]["stats"]["score"]
				temp_total += temp_result["report"]["stats"]["full_score"]
		
		if len(u.solutions):
			user_dict[u.name] = {"num_exercises" : len(u.solutions),
								"score" : temp_score,
								"full" : temp_total}
		else:
			user_dict[u.name] = {"num_exercises" : 0,
								"score" : 0, "full" : 0}
			
		
	if dev:
		exercise_dict["ye8aiud93af"] = {"name" : "Hi there", "num_students" : 3,
										"average_score" : 4, "total_score" : 10}
		exercise_dict["ye8afah3af"] = {"name" : "Bye there", "num_students" : 12,
										"average_score" : 12, "total_score" : 15}
		user_dict["Sam"] = {"num_exercises" : 5, "score" : 19, "full" : 37}
		user_dict["Maserati"] = {"num_exercises" : 4, "score" : 14, "full" : 27}
		user_dict["Ian"] = {"num_exercises" : 12, "score" : 87, "full" : 100}
	
	return render_template('home.html', exercise_dict=exercise_dict, user_dict=user_dict)
	

@app.route("/all_users")
def all_users():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))

	if not users and not dev:
		return render_template("error.html", error="No users found")
	
	user_dict = {}
	for user in users:
		if not dev:
			temp_total_score = 0
			temp_total_full = 0
			temp_user_dict = {"num_exercises" : 0, "solutions" : {}}
				
			for s in user.solutions:
				temp_result = s.get_latest_result()
				if temp_result:
					temp_report = temp_result["report"]
					temp_user_dict["solutions"][s.id] = {"exercise_name": s.exercise.name,
						"report": temp_report}
					temp_user_dict["num_exercises"] += 1
					
					temp_total_score += temp_result["report"]["stats"]["score"]
					temp_total_full += temp_result["report"]["stats"]["full_score"]
				else:
					temp_user_dict["solutions"][s.id] = {"exercise_name": s.exercise.name,
						"report": None}
					
					
						
			temp_user_dict["total_score"] = temp_total_score
			temp_user_dict["total_full"] = temp_total_full
			
			if temp_user_dict["num_exercises"] > 0:
				temp_user_dict["average_score"] = temp_total_score/temp_user_dict["num_exercises"]
				temp_user_dict["average_full"] = temp_total_full/temp_user_dict["num_exercises"]
			else:
				temp_user_dict["average_score"] = 0
				temp_user_dict["average_full"] = 0
				
		
		user_dict[user.name] = temp_user_dict
			
	if dev:
		temp_user_dict = {"num_exercises" : 12, "solutions" : {}, "total_score": 99,
		 "total_full": 100, "average_score": 9.9, "average_full": 10}
		temp_user_dict["solutions"]["aqts4q"] = {"exercise_name" : "Say Hello",
			"report": None}
		temp_user_dict["solutions"]["a4ad433q"] = {"exercise_name" : "Say Goodbye",
			"report": {"stats": {"score": 50, "full_score": 100},
			 "tests": {"bye": {"score": 49, "full_score": 50}} }}
		
		user_dict["Sam"] = temp_user_dict
	
	return render_template('all_users.html', user_dict=user_dict)
	

@app.route("/user")
def user():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
	
	user = None
	for u in users:
		if u.name == request.args["username"]:
			user = u
	
	if not user and not dev:
		return render_template("error.html", error="User not found")
	
	if not dev:
		temp_total_score = 0
		temp_total_full = 0
		user_dict = {"name" : user.name, "num_exercises" : 0, "solutions" : {}}
		
		for s in user.solutions:
			temp_result = s.get_latest_result()
			if temp_result:
				temp_report = temp_result["report"]
				user_dict["solutions"][s.id] = {"exercise_name": s.exercise.name,
					"report": temp_report}
				user_dict["num_exercises"] += 1
				
				temp_total_score += temp_result["report"]["stats"]["score"]
				temp_total_full += temp_result["report"]["stats"]["full_score"]
				
			else:
				user_dict["solutions"][s.id] = {"exercise_name": s.exercise.name,
					"report": None}
		
		user_dict["total_score"] = temp_total_score
		user_dict["total_full"] = temp_total_full
		
		if user_dict["num_exercises"] > 0:
			user_dict["average_score"] = temp_total_score/user_dict["num_exercises"]
			user_dict["average_full"] = temp_total_full/user_dict["num_exercises"]
		else:
			user_dict["average_score"] = 0
			user_dict["average_full"] = 0
			return render_template("error.html", error="No submissions found for this user") 
		
	if dev:
		user_dict = {"name" : "Sam", "num_exercises" : 12, "solutions" : {}, "average_score": 9.9,
		 "average_full": 10, "total_score": 99, "total_full": 100}
		user_dict["solutions"]["aqts4q"] = {"exercise_name" : "Say Hello",
			"report": None}
		user_dict["solutions"]["a4ad433q"] = {"exercise_name" : "Say Goodbye",
			"report": {"stats": {"score": 50, "full_score": 100},
			 "tests": {"bye": {"score": 49, "full_score": 50}} }}
	
	return render_template("user.html", user_dict=user_dict)

@app.route("/all_exercises")
def all_exercises():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
	
	if not exercises and not dev:
		return render_template("error.html", error="No exercises found")
	
	exercise_dict = {}
	
	for exercise in exercises:
		if not dev:
			temp_total_score = 0
			temp_total_full = 0
			temp_exercise_dict = {"name" : exercise.name,
				"num_students" : len(exercise.solutions), "solutions" : {}}
	
			for s in exercise.solutions:
				temp_result = s.get_latest_result()
				if temp_result:
					temp_exercise_dict["solutions"][s.id] = {"student_name" : s.user.name,
						"report": temp_result["report"]}
				else:
					temp_exercise_dict["solutions"][s.id] = {"student_name" : s.user.name,
																"report": None}
						
				temp_total_score += temp_result["report"]["stats"]["score"]
				temp_total_full += temp_result["report"]["stats"]["full_score"]
			
			if len(exercise.solutions):
				temp_exercise_dict["average_score"] = temp_total_score/len(exercise.solutions)
				temp_exercise_dict["average_full"] = temp_total_full/len(exercise.solutions)
			else:
				temp_exercise_dict["average_score"] = 0
				temp_exercise_dict["average_full"] = 0
				
			exercise_dict[exercise.id] = temp_exercise_dict
		
	if dev:
		temp_exercise_dict = {"name": "Hi There", "num_students": 5, "solutions": {},
									"average_score": 99,
		 "average_full": 100}
		temp_exercise_dict["solutions"]["aqts4q"] = {"student_name": "Sam", "report": None}
		temp_exercise_dict["solutions"]["a4ad433q"] = {"student_name": "Fitz",
			"report": {"stats": {"score": 50, "full_score": 100},
			 "tests": {"bye": {"score": 49, "full_score": 50}} }}
		
		exercise_dict["jd7ai4hurwkj"] = temp_exercise_dict
		
		temp_exercise_dict = {"name": "Bye there", "num_students": 4, "solutions": {},
									"average_score": 64,
		 "average_full": 136}
		temp_exercise_dict["solutions"]["aqsfhsfdq"] = {"student_name": "Ian", "report": None}
		temp_exercise_dict["solutions"]["a4ad4a54s"] = {"student_name": "Maserati",
			"report": {"stats": {"score": 54, "full_score": 124},
			 "tests": {"bye": {"score": 14, "full_score": 64}} }}
		
		exercise_dict["jd7f4fw4wkj"] = temp_exercise_dict
		
		
	
	return render_template('all_exercises.html', exercise_dict=exercise_dict)


@app.route("/exercise")
def exercise():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
	
	exercise = None
	for e in exercises:
		if e.id == request.args["id"]:
			exercise = e
	
	if not exercise and not dev:
		return render_template("error.html", error="Exercise not found")
	
	if not dev:
		exercise_dict={"name": exercise.name, "id":exercise.id, "solutions": {}}
		
		try:
			temp_solution = exercise.solutions[0]
		except:
			return render_template("error.html", error="No solutions found for this exercise") 
		
		temp_report = None
		for s in exercise.solutions:
			temp_report = s.get_latest_result()["report"]
			if temp_report:
				break
		
		if not temp_report:
			return render_template("error.html", error="No reports found for any solution") 
			
		
		full_score = temp_report["stats"]["full_score"]
		exercise_dict["full_score"] = full_score
		temp_tests = {}
		
		for test in temp_report["tests"]:
			temp_tests[test] = {"full_score": temp_report["tests"][test]["full_score"],
								"description": temp_report["tests"][test]["description"],
								"sum_score": 0, "num_attempts": 0, "average_score": 0}
		
		distrib_array = []
		for i in range(full_score+1):
			distrib_array.append(0)
		
		temp_sum_score = 0
		temp_num_attempts = 0
		for s in exercise.solutions:
			temp_result = s.get_latest_result()
			if not temp_result:
				exercise_dict["solutions"][s.id] = {"student_name" : s.user.name, "report": None}
				continue;
			
			exercise_dict["solutions"][s.id] = {"student_name" : s.user.name,
				"report": temp_result["report"]}
			
			distrib_array[temp_result["report"]["stats"]["score"] ] += 1
			temp_sum_score += temp_result["report"]["stats"]["score"]
			temp_num_attempts += 1
			
			for test in temp_result["report"]["tests"]:
				temp_tests[test]["num_attempts"] += 1
				temp_tests[test]["sum_score"] += temp_result["report"]["tests"][test]["score"]
		
		for test in temp_tests:
			temp_tests[test]["average_score"] = temp_tests[test]["sum_score"]*1.0/\
												temp_tests[test]["num_attempts"]
		
		exercise_dict["tests"] = temp_tests
		exercise_dict["distribution"] = distrib_array
		exercise_dict["num_attempts"] = temp_num_attempts
		exercise_dict["sum_score"] = temp_sum_score
		if temp_num_attempts > 0:
			exercise_dict["average_score"] = temp_sum_score*1.0/temp_num_attempts
		else:
			exercise_dict["average_score"] = 0
		
		
	if dev:
		# 5 students scored 0/6, 8 students scored 1/6, 3 students scored 2/6,... 
		exercise_dict = {"name": "Hi There", "id":"a74938ba9e9fb",
			"distribution":[5, 8, 3, 5, 11, 2, 1], "full_score":6, "average_score":5.3,
			"num_attempts": 35, "solutions": {}}
			
		temp_report = {"bye": {"average_score": 2, "full_score": 3,
									"description": "Write a code that says 'bye'"},
				 		"say bye": {"average_score": 1.2, "full_score": 3,
				 					"description": "Make sure your code says 'bye'"} }
				 
		exercise_dict["tests"] = temp_report
		
		exercise_dict["solutions"]["aqts4q"] = {"student_name": "Sam", "report": None}
		exercise_dict["solutions"]["a4ad433q"] = {"student_name": "Fitz",
			"report": {"stats": {"score": 50, "full_score": 100},
			 "tests": {"bye": {"score": 49, "full_score": 50}} }}
		
	return render_template('exercise.html', exercise_dict=exercise_dict)


@app.route("/codes")
def codes():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
	
	solutions_for_codes = []
	
	if not dev:
		if not request.args["all"]:
			for s in solutions:
				if s.id == request.args["id"]:
					solutions_for_codes.append(s)
		elif request.args["all"] == "user":
			for u in users:
				if u.name == request.args["id"]:
					temp_user = u
			for s in temp_user.solutions:
				solutions_for_codes.append(s)
		elif request.args["all"] == "exercise":
			for e in exercises:
				if e.id == request.args["id"]:
					temp_exercise = e
			for s in temp_exercise.solutions:
				solutions_for_codes.append(s)
		else:
			return render_template("error.html",
					error="codes?all="+request.args["all"]+" not handled")
	
	
	path_to_codes = "sourcecodes"
	if os.path.exists(path_to_codes):
		shutil.rmtree(path_to_codes)
		
	os.makedirs(path_to_codes)
	
	def zip(path_to_folder):
		dir_name = os.path.basename(path_to_folder)
		zf = zipfile.ZipFile(dir_name + ".zip", "w")
		for root, dirs, files in os.walk(path_to_folder):
			for file in files:
				zf.write(os.path.join(path_to_folder, file))
			zf.close()
	
	if not dev:
		for solution in solutions_for_codes:
			temp_path = Get_sourcecodes_from_solution(solution)
			os.rename(temp_path, path_to_codes+"/"+temp_path)
		
		zip(path_to_codes)
		zip_name = os.path.basename(path_to_codes)+".zip"
		os.rename(zip_name, app.config['UPLOAD_FOLDER']+"/"+zip_name)
		codes_dict = {"path_to_zip": path_to_codes+".zip"}
	
	if dev:
		temp_code = "This is some code\nfor the first file\nhere's what I recieved:\n"
		temp_code += "\tall = " + request.args["all"] + "; id = " + request.args["id"]
		file_name = "first_file"
		code_file = open(file_name+".cpp", 'w+')
		code_file.write(temp_code)
		code_file.close()
		temp_path = file_name+".cpp"
		os.rename(temp_path, path_to_codes+"/"+temp_path)
		
		temp_code = "Here is some more code\nIt should be in the 2nd file"
		file_name = "second_file"
		code_file = open(file_name+".cpp", 'w+')
		code_file.write(temp_code)
		code_file.close()
		temp_path = file_name+".cpp"
		os.rename(temp_path, path_to_codes+"/"+temp_path)
		
		temp_code = "Yet another file\nNumber 3"
		file_name = "third_file"
		code_file = open(file_name+".cpp", 'w+')
		code_file.write(temp_code)
		code_file.close()
		temp_path = file_name+".cpp"
		os.rename(temp_path, path_to_codes+"/"+temp_path)
		
		zip(path_to_codes)
		zip_name = os.path.basename(path_to_codes)+".zip"
		os.rename(zip_name, app.config['UPLOAD_FOLDER']+"/"+zip_name)
		codes_dict = {"path_to_zip": path_to_codes+".zip"}
	
	
	shutil.rmtree(path_to_codes)
		
	return send_from_directory(app.config['UPLOAD_FOLDER'], zip_name)


@app.route("/generate_report")
def generate_report():
	try:
		if not session['logged_in']:
			return redirect(url_for("login"))
	except:
		return redirect(url_for("login"))
	
	if not dev:
		for s in solutions:
			if s.id == request.args["id"]:
				solution = s
		
		if solution.get_latest_result():
			return render_template("error.html",
					error="solution with id="+solution.id+" already has a report")
		else:
			Generate_results(solution)

	if dev:
		temp_message = "flash message: generating result for id=" + request.args["id"]
		flash(temp_message)
	
	return redirect(request.args["return_url"])

	
	
if __name__ == "__main__":
	app.run(host=HOST)





