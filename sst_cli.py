"""
	This is the scriptable version of SST. It uses student_data.py for getting its data,
	but allows all functionality to be available using command line options.
"""

from student_data import *

if __name__ == '__main__':
	parser = argparse.ArgumentParser() 
	parser.add_argument("-u", "--username", help="Instructor Username") 
	parser.add_argument("-p", "--password", help="Instructor Password") 
	temptext = "Store data in a file called data.txt in current directory: "
	temptext += os.path.dirname(os.path.realpath(__file__)) + "/"
	parser.add_argument("-d", "--datafile", help=temptext, action="store_true") 
	parser.add_argument("-r", "--reports", help="Print reports", action="store_true") 
	temptext = "Retrieve source codes and store them in the current directory: "
	temptext += os.path.dirname(os.path.realpath(__file__)) + "/"
	parser.add_argument("-c", "--sourcecodes", help=temptext, action="store_true") 
	parser.add_argument("-cr", "--createresults", help="Create Results if they are not already in the database",
																								action="store_true") 
	
	parser.add_argument("-e", "--exercise", help="Get data for a specific exercise") 
	parser.add_argument("-ae", "--allexercises", help="Get data for all exercises", action="store_true") 
	parser.add_argument("--user", help="Get data for a specific user") 
	parser.add_argument("-au", "--allusers", help="Get data for all specific users", action="store_true") 
	
	args = parser.parse_args() 


def cli(args):
	username = args.username
	password = args.password
	
	
	# Handling possible errors
	if not username or not password:
		sys.exit("Error: No username or password entered. Use --username and --password")
	
	if args.exercise and args.user: 
		sys.exit("Error: Incompatible options --exercise and --user") 
		
	if args.exercise and args.allexercises: 
		sys.exit("Error: Incompatible options --exercise and --allexercises") 
	 
	if args.user and args.allusers: 
		sys.exit("Error: Incompatible options --user and --allusers") 
	 
	if args.allexercises and args.allusers: 
		sys.exit("Error: Incompatible options --allexercises and --allusers") 
	
	Populate_exercise_list(username, password)
	Populate_data(exercises)
	
	
	
	if args.datafile:
		data_file = open("data.txt", 'w+')
	
	
	if (solutions_wo_results):
		print "No results or reports were found for the following solutions"
		for s in solutions_wo_results:
			print "\t", "Submission to ", s.exercise.name, " submitted by ", s.user.name
		
		if args.createresults:
		 	for s in solutions_wo_results:
				print "Generating result for submission to ", s.exercise.name, " submitted by ", s.user.name
				Generate_results(s)
	
	if not args.user and not args.allusers:
		if args.exercise:
			exercise_name = args.exercise
			exercise_name = exercise_name.lower().lstrip().rstrip()
		
		if not args.exercise or args.exercise == "*" or args.allexercises:
			exercise_name = ""
			print "\nDisplaying data for all exercises"
			
			for exercise in exercises:
				print exercise.name, ":"
				
				latest_solutions = exercise.solutions
				num_attempted = len(latest_solutions)
				print num_attempted, " users attempted this exercise."
				
				num_full_score = 0
				num_zero = 0
				num_between = 0
				for s in latest_solutions:
					if s.get_latest_result():
						if s.get_latest_result()['report']:
							temp_report = s.get_latest_result()['report']
						else:
							print "no report found for " + s.user.name + " for this exercise"
							continue
					else:
						print "no result found for " + s.user.name + " for this exercise"
						continue
				
					if temp_report['stats']['score'] >= temp_report['stats']['full_score']:
						num_full_score += 1
					elif temp_report['stats']['score'] == 0:
						num_zero += 1
					else:
						num_between += 1
				
				print "\tFull score: ", num_full_score, " users"
				print "\tBetween zero and full score: ", num_between, " users"
				print "\tZero: ", num_zero, " users\n"
	
		else :
			exercise = None
			for e in exercises:
				if e.name.lower().lstrip().rstrip() == exercise_name:
					exercise = e
			if not exercise:
				print "Error getting exercise"
			print "\nDisplaying data for ", exercise.name, ":"
			
			latest_solutions = exercise.solutions
			num_attempted = len(latest_solutions)
			print num_attempted, " users attempted this exercise."
			
			num_full_score = 0
			num_zero = 0
			num_between = 0
			for s in latest_solutions:
				if s.get_latest_result():
					temp_report = s.get_latest_result()['report']
				else:
					print "no results found for " + s.user.name + " for this exercise"
					continue
				
				temp_report = s.get_latest_result()['report']
				if temp_report['stats']['score'] >= temp_report['stats']['full_score']:
					num_full_score += 1
				elif temp_report['stats']['score'] == 0:
					num_zero += 1
				else:
					num_between += 1
			
			print "\tFull score: ", num_full_score, " users"
			print "\tBetween zero and full score: ", num_between, " users"
			print "\tZero: ", num_zero, " users\n"	
	
	
	else:
		if args.user:
			user_name = args.user
			user_name = user_name.lower().lstrip().rstrip()
			
		if args.allusers or args.user == "*" or not args.user:
			user_name = ""
			print "\nDisplaying data for all users"
			
			for user in users:
				print user.name, ":"
				
				latest_solutions = user.solutions
				"""
				print "contents of latest_solutions:"
				for l in latest_solutions:
					print "\t", "name of exercise = ", l.exercise.name
					print "\t", "results:"
					for r in l.results:
						print r
				"""
				num_attempted = len(latest_solutions)
				print "This user attempted ", num_attempted, " exercises."
				
				for s in latest_solutions:
					if not s.get_latest_result():
						print "no result found for " + s.exercise.name + " exercise for this student"
						continue
					if not s.get_latest_result()['report']:
						print "no report found for " + s.exercise.name + " exercise for this student"
						continue
		
					temp_score = s.get_latest_result()['report']['stats']['score']
					temp_full = s.get_latest_result()['report']['stats']['full_score']
					print "\t", s.exercise.name, " Score: ", temp_score, "/", temp_full
			
				print ""
			
		else :
			for u in users:
				if u.name.lower().lstrip().rstrip() == user_name:
					user = u
			print "\nDisplaying data for ", user.name, ":"
			
			latest_solutions = user.solutions
			num_attempted = len(latest_solutions)
			print "This user attempted ", num_attempted, " exercises."
			
			for s in latest_solutions:
				if not s.get_latest_result():
					print "no result found for " + s.exercise.name + " exercise for this student"
					continue 
				temp_score = s.get_latest_result()['report']['stats']['score']
				temp_full = s.get_latest_result()['report']['stats']['full_score']
				print "\t", s.exercise.name, " Score: ", temp_score, "/", temp_full
			
			print ""
	
		
	if args.sourcecodes:
		if not args.user and not args.allusers:
			# get all source codes for all exercises
			if not args.exercise or args.exercise == "*" or args.allexercises:
				print "Retrieving source codes for all exercises"
				for e in exercises:
					Get_sourcecodes_from_exercise(e)
			# get all source codes for "args.exercise"
			else:
				if exercise:
					print "Retrieving source codes for ", exercise.name
					Get_sourcecodes_from_exercise(exercise)
				else:
					print "Error finding chosen exercise"
		else:
			# get all source codes for all users
			if not args.user or args.user == "*" or args.allusers:
				print "Retrieving source codes for all users"
				for e in exercises:
					Get_sourcecodes_from_exercise(e)
			# get all source codes for "args.user"
			else:
				if user:
					print "Retrieving source codes for ", user.name
					for s in user.solutions:
						Get_sourcecodes_from_solution(s)
				else:
					print "Error finding chosen user"
		print "Source codes retrieved. They are in the current directory: "
		print os.path.dirname(os.path.realpath(__file__)), "/\n"
	
	if args.reports:
		if not args.user and not args.allusers:
			# get all reports for all exercises
			if not exercise_name:
				print "Displaying reports for all exercises\n"
				for e in exercises:
					print e.name, ":"
					for s in e.solutions:
						print Result_to_String(s.get_latest_result(), e.name, s.user.name)
						print ""
					print "\n" 
			# get all reports for "exercise_name"
			else:
				if exercise:
					print "Displaying reports for ", exercise.name
					for s in exercise.solutions:
						print Result_to_String(s.get_latest_result(), exercise.name, s.user.name)
						print ""
				else:
					print "Error finding chosen exercise"
		else:
			# get all reports for all users
			if not args.user or args.user == "*" or args.allusers:
				print "Displaying reports for all users\n"
				for u in users:
					print u.name, ":"
					for s in u.solutions:
						print Result_to_String(s.get_latest_result(), s.exercise.name, s.user.name)
						print ""
					print "\n"
				
			# get all source codes for "user_name"
			else:
				if user:
					print "Displaying reports for ",user.name, ":"
					for s in user.solutions:
						print Result_to_String(s.get_latest_result(), s.exercise.name, s.user.name)
						print ""
				else:
					print "Error finding chosen user"	
	
	if args.datafile:
		data_file.close()



if __name__ == '__main__':
	cli(args)


