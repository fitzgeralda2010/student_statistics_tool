from datetime import datetime

class User:
	def __init__(self, username=""):
		self.name = username
		self.exercises = []
		self.solutions = []
	
	def add_exercise(self, exercise):
		found = False
		for e in self.exercises:
			if e.id == exercise.id:
				found = True
		if not found:
			self.exercises.append(exercise)
		
		return not found
	
	def add_solution(self, solution):
		found = False
		for s in self.solutions:
			if s.id == solution.id:
				found = True
		if not found:
			self.solutions.append(solution)
		
		return not found
	
class Solution:
	def __init__(self):
		self.exercise = ""
		self.user = ""
		self.id = ""
		self.results = []
		self.time = datetime.min
		self.latest_result = None
	
	def get_latest_result(self):
		# < is earlier
		#datetime.strptime('2014-06-15 07:31:41.789846', '%Y-%m-%d %H:%M:%S.%f')
		"""
		latest_datetime = datetime.min
		latest_result = None
		
		for r in self.results:
			if datetime.strptime(r['datetime'], '%Y-%m-%d %H:%M:%S.%f') > latest_datetime:
				latest_datetime = datetime.strptime(r['datetime'], '%Y-%m-%d %H:%M:%S.%f')
				latest_result = r
		
		self.time = latest_datetime
		self.latest_result = latest_result
		"""
		if len(self.results) > 0:
			return self.results[0]
		else:
			return None
#		return latest_result

		
class Exercise:
	def __init__(self, exercise_name=""):
		self.name = exercise_name
		self.users = []
		self.solutions = []
		self.id = ""
		self.ac_token = ""
	
	def add_solution(self, solution):
		found = False
		for s in self.solutions:
			if s.id == solution.id:
				found = True
		if not found:
			self.solutions.append(solution)
		
		return not found

	def add_user(self, user):
		found = False
		for u in self.users:
			if u.name == user.name:
				found = True
		if not found:
			self.users.append(user)
		
		return not found
	
		
		
		
		
		
		
		
		
		
		
		
		
	
	

